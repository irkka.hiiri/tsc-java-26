package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractModel {

    @NotNull
    private String id = NumberUtil.generateId();

}
