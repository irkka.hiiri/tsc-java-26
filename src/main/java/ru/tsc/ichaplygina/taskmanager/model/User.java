package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.DELIMITER;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@NoArgsConstructor
@Getter
@Setter
public final class User extends AbstractModel {

    @NotNull
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @NotNull
    private String login;

    @Nullable
    private String middleName;

    @NotNull
    private String passwordHash;

    @NotNull
    private Role role;

    private boolean locked;

    public User(@NotNull final String login, @NotNull final String passwordHash, @NotNull final String email, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.role = role;
        this.locked = false;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @NotNull final String email, @Nullable final String firstName,
                @Nullable final String middleName, @Nullable final String lastName, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.role = role;
        this.locked = false;
    }

    public final String getFullName() {
        return (isEmptyString(firstName) ? "" : firstName + " ") +
                (isEmptyString(middleName) ? "" : middleName + " ") +
                (isEmptyString(lastName) ? "" : lastName + " ");
    }

    @Override
    public final String toString() {
        return getId() + DELIMITER + login + DELIMITER + email + DELIMITER + "Role: " + role +
                (isEmptyString(getFullName()) ? "" : DELIMITER + "Full Name: " + getFullName());
    }

}
