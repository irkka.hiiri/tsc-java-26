package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.List;

public interface IService<E extends AbstractModel> {

    void add(@NotNull E entity);

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    int getSize();

    boolean isEmpty();

    @Nullable
    E removeById(@NotNull String id);
}
